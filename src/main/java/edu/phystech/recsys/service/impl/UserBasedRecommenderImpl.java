package edu.phystech.recsys.service.impl;

import edu.phystech.recsys.repository.impl.UserItemInteraction;
import edu.phystech.recsys.service.UserBasedRecommender;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;


@Service
public class UserBasedRecommenderImpl implements UserBasedRecommender {

    private UserItemInteraction interaction;

    @Value("${topK:15}")
    private int topK;

    @Override
    @Cacheable("userBasedUserRecommendation")
    public List<Pair<Long, Double>> getRecommendationsForUser(Long userId) {
        return getRecommendations(userId);
    }

    double getUserAvgRating(Long userId) {
        var usersPref = interaction.getInteraction().get(userId);
        double usersAvg = 0.;
        for (var elem : usersPref.entrySet()) {
            usersAvg += elem.getValue();
        }
        usersAvg /= usersPref.size();
        return usersAvg;
    }

    List<Pair<Long, Double>> getRecommendations(Long userId) {
        List<Pair<Long, Double>> similarUsers = getSimilarUsers(userId, 2);
        var usersRating = interaction.getInteraction().get(userId);
        double userAvg = getUserAvgRating(userId);

        List<Pair<Long, Double>> recommendations = new ArrayList<>();

        for (var item : interaction.getItems()) {
            if (usersRating.containsKey(item)) continue;

            int counter = 0;
            double norm = 0.0;
            double total = 0.0;

            for (int i = 0; i < similarUsers.size(); i++) {
                if (!interaction.getInteraction().get(similarUsers.get(i).getKey()).containsKey(item)) continue;

                Double cosineScore = similarUsers.get(i).getValue();
                Long otherId = similarUsers.get(i).getKey();

                double otherAvg = getUserAvgRating(otherId);
                double rating = interaction.getInteraction().get(similarUsers.get(i).getKey()).get(item);

                counter++;
                norm += Math.abs(cosineScore);
                total += (rating - otherAvg) * cosineScore;
            }

            double predRating = userAvg + ((counter == 0) || (norm == 0) ? 0 : (total / norm));
            recommendations.add(Pair.of(item, predRating));
        }
        recommendations.sort(Comparator.comparing(p -> -p.getRight()));
        return recommendations
                .stream()
                .limit(topK)
                .collect(Collectors.toList());
    }


    List<Pair<Long, Double>> getSimilarUsers(Long userId, int topK) {
        TreeMap<Double, Long> values = new TreeMap<>(Collections.reverseOrder());
        for (var elem : interaction.getInteraction().entrySet()) {
            var otherId = elem.getKey();
            if (userId.equals(otherId)) continue;
            var similarity = getSimilarityBetweenUsers(userId, otherId);
            if (similarity != -100) {
                values.put(similarity, otherId);
            }
        }
        topK = Math.min(values.size(), topK);

        List<Pair<Long, Double>> users = new ArrayList<>();
        for (var elem : values.entrySet()) {
            users.add(Pair.of(elem.getValue(), elem.getKey()));
            if (users.size() >= topK) break;
        }
        return users;
    }


    double getSimilarityBetweenUsers(Long userId, Long otherId) {
        var usersPref = interaction.getInteraction().get(userId);
        var othersPref = interaction.getInteraction().get(otherId);

        double usersAvg = 0.;
        double othersAvg = 0.;

        for (var elem : usersPref.entrySet()) {
            usersAvg += elem.getValue();
        }

        for (var elem : othersPref.entrySet()) {
            othersAvg += elem.getValue();
        }

        usersAvg /= usersPref.size();
        othersAvg /= othersPref.size();

        double usersDivisor = 0.;
        double othersDivisor = 0.;
        double sum = 0.;

        double userValue = 0.;
        double otherValue = 0.;
        long num = 0;

        for (var elem : usersPref.entrySet()) {
            if (!othersPref.containsKey(elem.getKey())) continue;
            userValue = elem.getValue() - usersAvg;
            otherValue = othersPref.get(elem.getKey()) - othersAvg;

            sum += userValue * otherValue;
            usersDivisor += userValue * userValue;
            othersDivisor = otherValue * otherValue;
            num += 1;
        }

        if (num >= 2 && usersDivisor != 0 && othersDivisor != 0) {
            return sum / (Math.sqrt(usersDivisor * othersDivisor));
        }
        return -100;
    }

    public UserBasedRecommenderImpl(UserItemInteraction interaction) {
        this.interaction = interaction;
    }

}
