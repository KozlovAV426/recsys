package edu.phystech.recsys.service.impl;

import edu.phystech.recsys.repository.impl.UserItemInteraction;
import edu.phystech.recsys.service.ItemBasedRecommender;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ItemBasedRecommenderImpl implements ItemBasedRecommender {

    private UserItemInteraction interaction;

    private Map<Long, Map<Long, Double>> diff;
    private Map<Long, Map<Long, Integer>> freq;

    @Value("${base.topK:15}")
    private int topK;

    public ItemBasedRecommenderImpl(UserItemInteraction interaction) {
        this.interaction = interaction;
        this.diff = new HashMap<>();
        this.freq = new HashMap<>();

        initDiffMatrix();
    }


    double getItemSimilarity(Long itemId, Long otherId) {
        double sum = 0.;
        double itemDivisor = 0.;
        double otherDivisor = 0.;

        for (var entry: interaction.getInteraction().entrySet()) {
            if (!entry.getValue().containsKey(itemId) || !entry.getValue().containsKey(otherId)) continue;

            double itemValue = entry.getValue().get(itemId);
            double otherValue = entry.getValue().get(otherId);

            sum += itemValue * otherValue;

            itemDivisor += itemValue * itemValue;
            otherDivisor += otherValue * otherValue;
        }

        if (itemDivisor == 0 || otherDivisor == 0) {
            return -100;
        }

        return sum / (Math.sqrt(itemDivisor * otherDivisor));
    }


    @Override
    @Cacheable("getSimilarItems")
    public List<Pair<Long, Double>> getSimilarItems(Long itemId) {

        return interaction.getItems()
                .stream()
                .map(id -> Pair.of(id, getItemSimilarity(itemId, id)))
                .sorted(Comparator.comparing(p -> -p.getValue()))
                .limit(topK)
                .collect(Collectors.toList());
    }


    void initDiffMatrix() {
        for (var userItem : interaction.getInteraction().entrySet()) {
            for (var item : userItem.getValue().entrySet()) {

                if (!diff.containsKey(item.getKey())) {
                    diff.put(item.getKey(), new HashMap<>());
                    freq.put(item.getKey(), new HashMap<>());
                }

                for (var elem: userItem.getValue().entrySet()) {
                    int prevCount = 0;

                    if (freq.get(item.getKey()).containsKey(elem.getKey())) {
                        prevCount = freq.get(item.getKey()).get(elem.getKey());
                    }

                    double prevDiff = 0.;
                    if (diff.get(item.getKey()).containsKey(elem.getKey())) {
                        prevDiff = diff.get(item.getKey()).get(elem.getKey());
                    }
                    double additionalDiff = item.getValue() - elem.getValue();
                    freq.get(item.getKey()).put(elem.getKey(), prevCount + 1);
                    diff.get(item.getKey()).put(elem.getKey(), prevDiff + additionalDiff);
                }
            }
        }
        double sum = 0.;
        int count = 0;
        for (Long itemId : diff.keySet()) {
            for (Long knownItem : diff.get(itemId).keySet()) {
                sum = diff.get(itemId).get(knownItem);
                count = freq.get(itemId).get(knownItem);
                diff.get(itemId).put(knownItem, sum / count);
            }
        }
    }


    Map<Long, Double> getItemsForUser(Long userId) {

        Map<Long, Double> uPred = diff.keySet().stream()
                .collect(Collectors.toMap(x -> x,  x -> 0.));
        Map<Long, Integer> uFreq = diff.keySet().stream()
                .collect(Collectors.toMap(x -> x,  x -> 0));

        for (Long other : interaction.getInteraction().get(userId).keySet()) {
            for (Long id : diff.keySet()) {
                if (!(diff.containsKey(id) && diff.get(id).containsKey(other)
                        && interaction.getInteraction().containsKey(userId) &&
                        interaction.getInteraction().get(userId).containsKey(other))) continue;
                double predictedValue = diff.get(id).get(other) + interaction.getInteraction().get(userId).get(other);

                if (!(uPred.containsKey(id) && uFreq.containsKey(id) &&
                        freq.containsKey(id) && freq.get(id).containsKey(other))) continue;

                double finalValue = predictedValue * freq.get(id).get(other);
                uPred.put(id, uPred.get(id) + finalValue);
                uFreq.put(id, uFreq.get(id) + freq.get(id).get(other));
            }
        }

        HashMap<Long, Double> occurrences = new HashMap<>();

        for (var id : uPred.keySet()) {
            if (uFreq.get(id) > 0) {
                occurrences.put(id, uPred.get(id) / uFreq.get(id));
            }
        }
        for (var item : interaction.getItems()) {
            if (interaction.getInteraction().get(userId).containsKey(item)) {
                occurrences.put(item, interaction.getInteraction().get(userId).get(item));
            } else if (!occurrences.containsKey(item)) {
                occurrences.put(item, -1.0);
            }
        }
        return occurrences;
    }

    @Override
    @Cacheable(value = "popularItems")
    public List<Pair<Long, Double>> getPopularItems(Integer topK) {
        return interaction.getPopularItems(topK);
    }


    @Override
    @Cacheable(value="boughtTogether")
    public List<Pair<Long, Integer>> getFrequentlyBoughtTogether(Long itemId) {
        return interaction.getBoughtTogether(itemId, topK);
    }


    @Override
    @Cacheable(value = "itemBasedUserRecommendation")
    public List<Pair<Long, Double>> getRecommendationsForUser(Long userId) {
        var items = getItemsForUser(userId);

        var result = items.entrySet().stream()
                .sorted(Comparator.comparing(p -> -p.getValue()))
                .limit(topK)
                .map(x -> Pair.of(x.getKey(), x.getValue()))
                .collect(Collectors.toList());

        return result;
    }
}
