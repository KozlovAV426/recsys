package edu.phystech.recsys.service;

import org.apache.commons.lang3.tuple.Pair;

import java.util.List;

public interface UserBasedRecommender {
    List<Pair<Long, Double>> getRecommendationsForUser(Long userId);
}
