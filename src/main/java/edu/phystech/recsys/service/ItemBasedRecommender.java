package edu.phystech.recsys.service;

import org.apache.commons.lang3.tuple.Pair;

import java.util.List;

public interface ItemBasedRecommender {
    List<Pair<Long, Double>> getRecommendationsForUser(Long userId);

    List<Pair<Long, Double>> getSimilarItems(Long itemId);

    List<Pair<Long, Integer>> getFrequentlyBoughtTogether(Long itemId);

    List<Pair<Long, Double>> getPopularItems(Integer topK);
}
