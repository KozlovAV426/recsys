package edu.phystech.recsys.repository;

import org.apache.commons.lang3.tuple.Pair;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface Interaction {
    List<Pair<Long, Double>> getPopularItems(int topK);

    List<Pair<Long, Integer>> getBoughtTogether(Long itemId, int topK);

    Map<Long, HashMap<Long, Double>> getInteraction();

    List<Long> getItems();
}
