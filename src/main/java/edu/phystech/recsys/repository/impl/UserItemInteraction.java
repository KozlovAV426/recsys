package edu.phystech.recsys.repository.impl;

import edu.phystech.recsys.repository.Interaction;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.*;

@Repository
public class UserItemInteraction implements Interaction {

    private static final String SELECT_QUERY = "select * from ";
    private static final String POPULAR_ITEMS_QUERY = "select %s, avg(%s) as avg, count(%s)  as cnt " +
            "from %s group by %s order by avg desc, cnt desc;";

    private static final String BOUGHT_TOGETHER_QUERY = "select main, other, count(*) as cnt from (select a.%s as main," +
            " b.%s as other from %s as a inner join %s as b on a.%s = b.%s and a.%s != b.%s where a.%s=%d) as tmp\n" +
            "group by main, other order by cnt desc limit %d";

    @Value("${user.table_name}")
    private String tableName;

    @Value("${user.user_column}")
    private String userColumn;

    @Value("${user.item_column}")
    private String itemColumn;

    @Value("${user.rating_column}")
    private String ratingColumn;


    @Value("${base.limiter:30}")
    private int limiter;

    private Map<Long, HashMap<Long, Double>> data;
    private Set<Long> items;

    private JdbcTemplate jdbcTemplate;

    public UserItemInteraction(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        data = new HashMap<>();
        items = new TreeSet<>();
    }

    @Override
    public List<Pair<Long, Integer>> getBoughtTogether(Long itemId, int topK) {
        String query = String.format(BOUGHT_TOGETHER_QUERY, itemColumn, itemColumn, tableName, tableName,
                userColumn, userColumn, itemColumn, itemColumn, itemColumn, itemId, topK);

        List<Pair<Long, Integer>> top = new ArrayList<>();
        jdbcTemplate.query(query, rs -> {
            Long otherId = rs.getLong(2);
            Integer counter = rs.getInt(3);
            if (top.size() < topK) {
                top.add(Pair.of(otherId, counter));
            }
        });

        return top;
    }


    @Override
    public List<Pair<Long, Double>> getPopularItems(int topK) {
        String query = String.format(POPULAR_ITEMS_QUERY, itemColumn, ratingColumn, ratingColumn, tableName, itemColumn);

        List<Pair<Long, Double>> top = new ArrayList<>();
        jdbcTemplate.query(query, rs -> {
            Long itemId = rs.getLong(1);
            Double avg = rs.getDouble(2);
            if (top.size() < topK) {
                top.add(Pair.of(itemId, avg));
            }
        });

        return top;
    }

    @PostConstruct
    private void postConstruct() {
        jdbcTemplate.query(SELECT_QUERY + tableName, rs -> {
            Long userId = rs.getLong(userColumn);
            Long itemId = rs.getLong(itemColumn);
            Double rating = rs.getDouble(ratingColumn);

            items.add(itemId);

            if (!data.containsKey(userId)) {
                var map = new HashMap<Long, Double>();
                map.put(itemId, rating);
                data.put(userId, map);
            } else {
                if (data.get(userId).size() < limiter) {
                    data.get(userId).put(itemId, rating);
                }
            }
        });
    }

    @Override
    public Map<Long, HashMap<Long, Double>> getInteraction() {
        return data;
    }

    @Override
    public List<Long> getItems() {
        return new ArrayList<>(items);
    }
}
