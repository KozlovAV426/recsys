package edu.phystech.recsys.controller;

import edu.phystech.recsys.service.ItemBasedRecommender;
import edu.phystech.recsys.service.UserBasedRecommender;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class Controller {

    @Autowired
    UserBasedRecommender userBasedRecommender;

    @Autowired
    ItemBasedRecommender itemBasedRecommender;

    @GetMapping("user/user_based/{id}")
    public List<Pair<Long, Double>> getRecommendationByUser(@PathVariable Long id) {
        return userBasedRecommender.getRecommendationsForUser(id);
    }

    @GetMapping("user/item_based/{id}")
    public List<Pair<Long, Double>> getRecommendationForUser(@PathVariable Long id) {
        return itemBasedRecommender.getRecommendationsForUser(id);
    }

    @GetMapping("item/bought_together/{id}")
    public List<Pair<Long, Integer>> getFrequentlyBoughtTogether(@PathVariable Long id) {
        return itemBasedRecommender.getFrequentlyBoughtTogether(id);
    }

    @GetMapping("item/similar_items/{id}")
    public List<Pair<Long, Double>> getSimilarItems(@PathVariable Long id) {
        return itemBasedRecommender.getSimilarItems(id);
    }

    @GetMapping("item/popular_items/{topK}")
    public List<Pair<Long, Double>> getPopularItems(@PathVariable Integer topK) {
        return itemBasedRecommender.getPopularItems(topK);
    }

}
