# recsys



## Getting started

Для того, чтобы взаимодействовать с апи, нужно иметь `postgres таблицу` взаимодействий пользователей с товарами. Пользователю нужно передать `host` и `port` базы, а также `username` и `password` для подключения к базе.


Далее нужно передать следующие параметры:
 1. `user.table_name` - название таблицы со взаимодействиями
 2. `user.user_column` - название колонки с id пользователей
 3. `user.item_column` - название колонки с id товаров
 4. `user.rating_column` - название колонки с рейтингами (оценками)


Предполагается, что таблица со взаимодействиями имеет такой вид:
```
user_id |  item_id  | rating |
12      |  343      |   3.0  |
12      |  235      |   5.0  |
```

Запуск с передачей параметров
```
 mvn spring-boot:run -Dspring-boot.run.arguments=" --db.hostname=localhost --db.port=5432 --db.db_name=db_name --db.username=dbuser --db.password=dbuser --user.table_name=interaction --user.user_column=user_id --user.item_column=item_id --user.rating_column=rating"

```

## Usage
Апи:
  1. `api/user/user_based/{id}` - получить рекомендации по `id` пользователя (на основе пользователей)
  2. `api/user/item_based/{id}` - получить рекомендации по `id` пользователя (на основе товаров)
  3. `api/item/bought_together/{id}` - получить список "покупают вместе" по `id` товара
  4. `api/item/similar_items/{id}` - получить список похожих товаров по `id` товара
  5. `api/item/popular_items/{topK}` - получить список из `topK` популярных товаров.

